package com.example.FX.Rate.Service.service;

import com.example.FX.Rate.Service.dao.ExchangeDao;
import com.example.FX.Rate.Service.model.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ExchangeService {

    private final ExchangeDao exchangeDao;

    @Autowired
    public ExchangeService(@Qualifier("postgres") ExchangeDao exchangeDao) {
        this.exchangeDao = exchangeDao;
    }

    public List<Exchange> getAllExchangeRates(){
        return exchangeDao.selectAllExchangeRates();
    }
    public Optional<Exchange> getExchangeRateByBase(String base){
        return exchangeDao.selectExchangeRateByBase(base);
    }

}

