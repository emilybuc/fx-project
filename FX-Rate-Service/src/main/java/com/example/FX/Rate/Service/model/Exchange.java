package com.example.FX.Rate.Service.model;
import com.fasterxml.jackson.annotation.JsonProperty;


import java.sql.Date;

public class Exchange {
    private final String base;
    private final Date date;
    private final float GDP;
    private final float EUR;
    private final float USD;
    private final float AUD;
    private final float JPY;

    public Exchange(@JsonProperty("base") String base,
                    @JsonProperty("date") Date date,
                    @JsonProperty("GDP") float GDP,
                    @JsonProperty("EUR") float EUR,
                    @JsonProperty("USD") float USD,
                    @JsonProperty("AUD") float AUD,
                    @JsonProperty("JPY") float JPY) {
        this.base = base;
        this.date = date;
        this.GDP = GDP;
        this.EUR = EUR;
        this.USD = USD;
        this.AUD = AUD;
        this.JPY = JPY;
    }


    public String getBase() {
        return base;
    }

    public Date getDate() {
        return date;
    }

    public float getGDP() {
        return GDP;
    }

    public float getEUR() {
        return EUR;
    }

    public float getUSD() {
        return USD;
    }

    public float getAUD() {
        return AUD;
    }

    public float getJPY() {
        return JPY;
    }
}
