package com.example.FX.Rate.Service.dao;

import com.example.FX.Rate.Service.model.Exchange;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Repository("postgres")
public class ExchangeDataAccessService implements ExchangeDao{
    private final JdbcTemplate jdbcTemplate;

    public ExchangeDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Exchange> selectAllExchangeRates() {
        final String sql = "SELECT * FROM exchange";
            List<Exchange> exchange = jdbcTemplate.query(sql, (resultSet, i)->{
            String base = resultSet.getString("base");
            Date date = resultSet.getDate("date");
                float gdp = resultSet.getFloat("gdp");
                float eur = resultSet.getFloat("eur");
                float usd = resultSet.getFloat("usd");
                float aud = resultSet.getFloat("aud");
                float jpy = resultSet.getFloat("jpy");
            return new Exchange(
                    base,date,gdp,eur,usd,aud,jpy
            );
        });
        return exchange;
    }

    @Override
    public Optional<Exchange> selectExchangeRateByBase(String base) {
        final String sql = "SELECT base,date,gdp,eur,usd,aud,jpy FROM exchange WHERE base = ?";

        Exchange exchange = jdbcTemplate.queryForObject(
                sql,
                new Object[]{base},
                (resultSet, i) -> {
            String exchangeBase = resultSet.getString("base");
            Date date = resultSet.getDate("date");
                    float gdp = resultSet.getFloat("gdp");
                    float eur = resultSet.getFloat("eur");
                    float usd = resultSet.getFloat("usd");
                    float aud = resultSet.getFloat("aud");
                    float jpy = resultSet.getFloat("jpy");
            return new Exchange(
                    exchangeBase,date,gdp,eur,usd,aud,jpy
            );
        });
        return Optional.ofNullable(exchange);
    }
}
