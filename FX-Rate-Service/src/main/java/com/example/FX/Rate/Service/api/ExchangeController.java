package com.example.FX.Rate.Service.api;

import com.example.FX.Rate.Service.model.Exchange;
import com.example.FX.Rate.Service.service.ExchangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("api/exchange")
@RestController
public class ExchangeController {
    private final ExchangeService exchangeService;
@Autowired
    public ExchangeController(ExchangeService exchangeService) {
        this.exchangeService = exchangeService;
    }

    @GetMapping
    public List<Exchange> getAllExchangeRates(){
    return exchangeService.getAllExchangeRates();
    }
    @GetMapping(path = "{base}")
    public Exchange getExchangeRateByBase(@PathVariable("base") String base){
    return exchangeService.getExchangeRateByBase(base)
            .orElse(null);
    }
}
