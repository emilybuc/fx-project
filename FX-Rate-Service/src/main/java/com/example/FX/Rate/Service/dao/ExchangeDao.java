package com.example.FX.Rate.Service.dao;
import com.example.FX.Rate.Service.model.Exchange;


import java.util.List;
import java.util.Optional;

public interface ExchangeDao {

    List<Exchange> selectAllExchangeRates();
    Optional<Exchange> selectExchangeRateByBase(String base);
}
